<?php

/**
 * @file
 * Admin page callbacks for the Fractal module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Settings form builder.
 */
function fractal_admin_settings() {
  $form = array();

  // Count cache and derivatives.
  $files = fractal_db_get_managed_files();
  $fids = array();
  $view_modes = variable_get('fractal_view_modes', array());
  $derivative_misses = 0;

  foreach ($files as $file) {
    $fids[] = $file->fid;
    foreach (fractal_get_derivatives($file, $view_modes) as $derivative) {
      if (!file_exists($derivative['destination'])) {
        $derivative_misses ++;
      }
    }
  }

  $optimum_misses = count($files) - fractal_db_count_multiple_by_fid($fids);

  $form['build'] = array(
    '#type' => 'fieldset',
    '#title' => t('Generate images'),
    '#description' => t('Optimum cache should be created before derivative images. This ensures that derivatives of the optimum files are created.'),
  );

  $form['build']['build_optimum_cache'] = array(
    '#type' => 'submit',
    '#value' => t('Create optimum cache (@count)', array(
      '@count' => $optimum_misses,
    )),
    '#submit' => array('fractal_admin_build_optimum_cache_submit'),
  );

  $form['build']['build_styles'] = array(
    '#type' => 'submit',
    '#value' => t('Create derivative images (@count)', array(
      '@count' => $derivative_misses,
    )),
    '#submit' => array('fractal_admin_build_styles_submit'),
  );

  $form['flush'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Flush images'),
  );

  $form['flush']['flush_optimum_cache'] = array(
    '#type' => 'submit',
    '#value' => t('Flush optimum cache'),
    '#submit' => array('fractal_admin_flush_optimum_cache_submit'),
  );

  $form['flush']['flush_styles'] = array(
    '#type' => 'submit',
    '#value' => t('Flush derivative images'),
    '#submit' => array('fractal_admin_flush_styles_submit'),
  );

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
  );

  $options = array();
  foreach (fractal_file_formatters() as $formatter) {
    $options[$formatter['name']] = $formatter['label'];
  }

  $form['settings']['fractal_view_modes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('View modes'),
    '#description' => t('<a href="!url">View modes of the Image file type</a> with Image or Picture displays that should be managed by Fractal.', array(
      '!url' => url('admin/structure/file-types/manage/image/file-display'),
    )),
    '#options' => $options,
    '#default_value' => variable_get('fractal_view_modes', array()),
  );

  return system_settings_form($form);
}

/**
 * Form validation handler.
 *
 * @see fractal_admin_settings()
 */
function fractal_admin_settings_validate($form, &$form_state) {
  // Save values of checked checkboxes only.
  $values = array();
  foreach ($form_state['values']['fractal_view_modes'] as $key => $value) {
    if (!empty($value)) {
      $values[] = $key;
    }
  }
  $form_state['values']['fractal_view_modes'] = $values;
}

/**
 * Form submit handler.
 *
 * @see fractal_admin_settings()
 */
function fractal_admin_flush_optimum_cache_submit($form, &$form_state) {
  fractal_flush_optimum_cache();

  drupal_set_message(t('Optimum cache flushed.'));
}

/**
 * Form submit handler.
 *
 * @see fractal_admin_settings()
 */
function fractal_admin_build_optimum_cache_submit($form, &$form_state) {
  $files = fractal_db_get_managed_files();

  if (empty($files)) {
    drupal_set_message('There are no files to process.');
  }
  else {
    foreach ($files as $file) {
      $operations[] = array('fractal_admin_build_optimum_cache_batch', array($file));
    }

    $batch = array(
      'title' => 'Creating optimum cache',
      'operations' => $operations,
      'finished' => 'fractal_admin_build_optimum_cache_batch_finished',
      'file' => drupal_get_path('module', 'fractal') . '/fractal.admin.inc',
    );
    batch_set($batch);
  }
}

/**
 * Batch operation.
 *
 * @param array $params
 * @param array $context
 * @see fractal_admin_build_optimum_cache_submit()
 */
function fractal_admin_build_optimum_cache_batch($params, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = 1;
  }

  $result = fractal_inspect_file($params);
  $context['results'][] = array($params, $result);
  $context['sandbox']['progress'] ++;

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch finished callback.
 *
 * @see fractal_admin_build_optimum_cache_submit()
 */
function fractal_admin_build_optimum_cache_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), '@count file in optimum cache.', '@count files in optimum cache.');
  }
  else {
    $error_operation = reset($operations);
    $message = 'An error occurred while creating ' . $error_operation[0] . ' with arguments :' . print_r($error_operation[0], TRUE);
  }

  drupal_set_message($message);
}

/**
 * Form submit handler.
 *
 * @see fractal_admin_settings()
 */
function fractal_admin_flush_styles_submit() {
  $operations = array();

  foreach (fractal_image_styles() as $style) {
    $operations[] = array('fractal_admin_flush_styles_batch', array($style));
  }

  if (empty($operations)) {
    drupal_set_message('There are no image styles to flush.');
  }
  else {
    $batch = array(
      'title' => 'Flushing image styles',
      'operations' => $operations,
      'finished' => 'fractal_admin_flush_styles_batch_finished',
      'file' => drupal_get_path('module', 'fractal') . '/fractal.admin.inc',
    );
    batch_set($batch);
  }
}

/**
 * Batch operation.
 *
 * @param array $params
 * @param array $context
 * @see fractal_admin_flush_styles_submit()
 */
function fractal_admin_flush_styles_batch($params, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = 1;
  }

  $result = fractal_flush_style($params);
  $context['results'][] = array($params, $result);
  $context['sandbox']['progress'] ++;

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch finished callback.
 *
 * @see fractal_admin_flush_styles_submit()
 */
function fractal_admin_flush_styles_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), '@count image style flushed.', '@count image styles flushed.');
  }
  else {
    $error_operation = reset($operations);
    $message = 'An error occurred while creating ' . $error_operation[0] . ' with arguments :' . print_r($error_operation[0], TRUE);
  }

  drupal_set_message($message);
}

/**
 * Form submit handler. Creates batch to generate derivative images.
 *
 * @see fractal_admin_settings()
 */
function fractal_admin_build_styles_submit() {
  $styles = fractal_image_styles();

  if (empty($styles)) {
    drupal_set_message('There are no image styles to process.');
  }
  else {
    $files = fractal_db_get_managed_files();
    $view_modes = variable_get('fractal_view_modes', array());

    if (empty($files)) {
      drupal_set_message('There are no files to process.');
    }
    else {
      $operations = array();

      foreach ($files as $file) {
        foreach (fractal_get_derivatives($file, $view_modes) as $derivative) {
          $operations[] = array('fractal_admin_build_styles_batch', array($derivative));
        }
      }

      $batch = array(
        'title' => 'Creating derivative images',
        'operations' => $operations,
        'finished' => 'fractal_admin_build_styles_batch_finished',
        'file' => drupal_get_path('module', 'fractal') . '/fractal.admin.inc',
      );
      batch_set($batch);
    }
  }
}

/**
 * Batch operation. Generates derivative images.
 *
 * @param array $params
 * @param array $context
 * @see fractal_admin_build_styles_submit()
 */
function fractal_admin_build_styles_batch($params, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = 1;
  }

  $result = fractal_style_create_derivative($params['image_style'], $params['source'], $params['destination']);
  $context['results'][] = $result;
  $context['sandbox']['progress'] ++;

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch finished callback. Finished generating derivative images.
 *
 * @see fractal_admin_build_styles_submit()
 */
function fractal_admin_build_styles_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), '@count derivative image processed.', '@count derivative images processed.');
  }
  else {
    $error_operation = reset($operations);
    $message = 'An error occurred while creating ' . $error_operation[0] . ' with arguments :' . print_r($error_operation[0], TRUE);
  }

  drupal_set_message($message);
}
