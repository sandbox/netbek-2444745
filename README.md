# fractal

Bulk generation of image styles. Fractal also inspects JPEG and PNG images and serves the format with the smallest file size.

## Configuration

Go to `/admin/config/media/fractal` and set the view modes.

## Usage

Replace Image functions with their Fractal equivalent to let Fractal decide whether to serve a JPEG or PNG version of an image.

```
image_style_path() -> fractal_style_path()
image_style_url()  -> fractal_style_url()
```

Images are inspected and derivatives are created when files are added to the database (`hook_file_insert()`). This can also be done manually at `/admin/config/media/fractal`.

## To do

* Add support for images embedded with WYSIWYG editor (`update fractal_get_derivatives()` accordingly)
* Add support for other toolkits in `fractal_save()`
