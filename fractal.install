<?php

/**
 * @file
 * Install, update and uninstall functions for the Fractal module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Implements hook_install()
 */
function fractal_install() {
  db_query("UPDATE {system} SET weight = 1 WHERE name = 'fractal'");

  // Create the Fractal directory and ensure it is writable.
  $directory = 'public' . '://' . FRACTAL_DIR;
  file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

  $quality = variable_get('image_jpeg_quality', 75);
  variable_set('fractal_image_jpeg_quality', $quality);
}

/**
 * Implements hook_uninstall()
 */
function fractal_uninstall() {
  module_load_include('module', 'fractal', 'fractal');

  // Delete all optimum files, derivatives of optimum files and the cron queue.
  fractal_flush_optimum_cache();

  $result = db_select('variable', 'v')
      ->fields('v', array('name'))
      ->condition('name', 'fractal_%', 'LIKE')
      ->execute();
  foreach ($result as $row) {
    variable_del($row->name);
  }
}

/**
 * Implements hook_requirements().
 */
function fractal_requirements($phase) {
  $requirements = array();
  $t = get_t();

  if ($phase == 'runtime') {
    $getid3_path = libraries_get_path('getid3');

    if (empty($getid3_path)) {
      $requirements['fractal_getid3'] = array(
        'title' => $t('Fractal'),
        'severity' => REQUIREMENT_ERROR,
        'value' => $t('!library library not found.', array('!library' => 'getID3')),
      );
    }
    else {
      $getid3_path .= '/getid3/getid3.php';

      if (!file_exists($getid3_path)) {
        $requirements['fractal_getid3'] = array(
          'title' => $t('Fractal'),
          'severity' => REQUIREMENT_ERROR,
          'value' => $t('!library library not found.', array('!library' => 'getID3')),
        );
      }
    }

    if (empty($requirements)) {
      $requirements['fractal'] = array(
        'title' => $t('Fractal'),
        'severity' => REQUIREMENT_OK,
        'value' => $t('OK'),
      );
    }
  }

  return $requirements;
}

/**
 * Implements hook_schema().
 */
function fractal_schema() {
  $schema['fractal'] = array(
    'description' => 'Stores information about tests for smallest file size in different image formats.',
    'fields' => array(
      'cid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary Key: Unique test ID.',
      ),
      'fid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'File ID.',
      ),
      'uri' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'URI to access the file.',
      ),
      'extension' => array(
        'type' => 'varchar',
        'length' => 5,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Extension of the best format.',
      ),
      'mimetype' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
        'description' => 'MIME type of the best format.',
      ),
      'created' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'UNIX timestamp for when the file was added.',
      ),
    ),
    'unique keys' => array(
      'fid' => array('fid'),
    ),
    'primary key' => array('cid'),
  );

  return $schema;
}
