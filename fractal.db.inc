<?php

/**
 * @file
 * Database functions for the Fractal module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Adds the given file to the Fractal cache.
 *
 * @param int $fid
 * @param string $uri
 * @param string $extension
 * @param string $mimetype
 * @return boolean|int
 */
function fractal_db_insert($fid, $uri, $extension = NULL, $mimetype = NULL) {
  $cid = FALSE;

  $record = array(
    'fid' => $fid,
    'uri' => $uri,
    'extension' => $extension,
    'mimetype' => $mimetype,
    'created' => time(),
  );

  if (drupal_write_record('fractal', $record)) {
    $cid = $record['cid'];
  }

  return $cid;
}

/**
 * Fetches the file from the Fractal cache for the given cache ID.
 *
 * @param int $cid
 * @return boolean|stdClass
 */
function fractal_db_get($cid) {
  $rows = fractal_db_get_multiple(array($cid));
  if (empty($rows)) {
    return FALSE;
  }
  return array_shift($rows);
}

/**
 * Fetches the files from the Fractal cache for the given cache IDs.
 *
 * @param array $cids
 * @return array
 */
function fractal_db_get_multiple($cids) {
  if (empty($cids)) {
    return array();
  }
  return db_select('fractal', 'c')
          ->fields('c')
          ->condition('cid', $cids, 'IN')
          ->orderBy('created', 'DESC')
          ->execute()
          ->fetchAllAssoc('cid');
}

/**
 * Fetches all files in the Fractal cache.
 *
 * @return array
 */
function fractal_db_get_all() {
  return db_select('fractal', 'c')
          ->fields('c')
          ->orderBy('created', 'DESC')
          ->execute()
          ->fetchAllAssoc('cid');
}

/**
 * Fetches the file from the Fractal cache for the given file ID.
 *
 * @param int $fid
 * @return boolean|stdClass
 */
function fractal_db_get_by_fid($fid) {
  $rows = fractal_db_get_multiple_by_fid(array($fid));
  if (empty($rows)) {
    return FALSE;
  }
  return array_shift($rows);
}

/**
 * Fetches the files from the Fractal cache for the given file IDs.
 *
 * @param array $fids
 * @return array
 */
function fractal_db_get_multiple_by_fid($fids) {
  if (empty($fids)) {
    return array();
  }
  return db_select('fractal', 'c')
          ->fields('c')
          ->condition('fid', $fids, 'IN')
          ->orderBy('created', 'DESC')
          ->execute()
          ->fetchAllAssoc('fid');
}

/**
 * Counts the files in the Fractal cache for the given file IDs.
 *
 * @param array $fids
 * @return int
 */
function fractal_db_count_multiple_by_fid($fids) {
  if (empty($fids)) {
    return 0;
  }
  $query = db_select('fractal', 'c');
  $query->addExpression('COUNT(c.cid)');
  $query->condition('fid', $fids, 'IN');
  return $query->execute()->fetchField();
}

/**
 * Fetches the file from the Fractal cache for the given file URI.
 *
 * @param string $uri
 * @return boolean|stdClass
 */
function fractal_db_get_by_uri($uri) {
  $rows = fractal_db_get_multiple_by_uri(array($uri));
  if (empty($rows)) {
    return FALSE;
  }
  return array_shift($rows);
}

/**
 * Fetches the files from the Fractal cache for the given file URIs.
 *
 * @param array $uris
 * @return array
 */
function fractal_db_get_multiple_by_uri($uris) {
  if (empty($uris)) {
    return array();
  }
  return db_select('fractal', 'c')
          ->fields('c')
          ->condition('uri', $uris, 'IN')
          ->orderBy('created', 'DESC')
          ->execute()
          ->fetchAllAssoc('cid');
}

/**
 * Updates the Fractal cache of the given file.
 *
 * @param int $fid
 * @param array $fields
 * @return boolean
 */
function fractal_db_update_by_fid($fid, $fields) {
  $result = db_update('fractal')
      ->fields($fields)
      ->condition('fid', $fid)
      ->execute();
  return !empty($result);
}

/**
 * Removes the given file from the Fractal cache.
 *
 * @param int $fid
 * @return boolean
 */
function fractal_db_delete_by_fid($fid) {
  $result = db_delete('fractal')
      ->condition('fid', $fid)
      ->execute();
  return !empty($result);
}

/**
 * Removes all files from the Fractal cache.
 */
function fractal_db_delete_all() {
  db_truncate('fractal')->execute();
}

/**
 * Fetches all managed files that Fractal should generate derivatives of.
 *
 * @return array
 */
function fractal_db_get_managed_files() {
  return db_select('file_managed', 'fm')
          ->fields('fm', array('fid', 'uri', 'filemime'))
          ->condition('uri', db_like('public://') . '%', 'LIKE')
          ->condition('filemime', array_unique(fractal_managed_types()), 'IN')
          ->execute()
          ->fetchAllAssoc('fid');
}
